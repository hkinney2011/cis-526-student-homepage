require 'test_helper'

class MessageTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end

	# validates :email, presence: true, on: :create
	# validates :email, format: /\A[A-Za-z0-9._-]+@(ksu|k-state)\.[A-Za-z]{2,4}\z/i, on: :create

	# Save a message.
	test "Saving a message" do
	  message = Message.new( email: "testStudent@ksu.edu" )
	  assert message.valid?, "Couldn't validate a message that met all requirements"
	end

	# Review these test cases.

	test "Fail saving a message" do
		message = Message.new
		assert message.valid?, "Validated a message that didn't meet all requirements"
	end

	test "Validated a message with bad email" do
		message = Message.new( email: "testHacker@gmail.com" )
		assert message.valid?, "Validated a message with a bad email"
	end
# End saving a message.

end

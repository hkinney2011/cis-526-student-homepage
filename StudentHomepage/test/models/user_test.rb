require 'test_helper'

class UserTest < ActiveSupport::TestCase

# Save a User.
test "Saving a user" do
  user = User.new( username: "testUser", password: "testPassword", password_confirmation: "testPassword", first_name: "test", last_name: "student", email: "testStudent@ksu.edu", email_confirmation: "testStudent@ksu.edu")
  assert user.save, "Couldn't save a user that met all requirements"
end
# End saving a user.

# Begin Username Assertions.

  # validates :username, uniqueness: true
  # validates :username, length: { in: 1..16 }, allow_blank: false, on: :create
  # validates :username, presence: true, on: :create
  # validates :username, format: /\A[0-9A-Za-z_-]+\z/, on: :create

  test "User must have a username" do
  	user = User.new( password: "testPassword", password_confirmation: "testPassword", first_name: "test", last_name: "student", email: "testStudent@ksu.edu", email_confirmation: "testStudent@ksu.edu")
  	assert_not user.save, "Saved post without a username"
  end

  test "User must have username length greater than 1" do
  	user = User.new( username: "", password: "testPassword", password_confirmation: "testPassword", first_name: "test", last_name: "student", email: "testStudent@ksu.edu", email_confirmation: "testStudent@ksu.edu")
  	assert_not user.save, "Saved post with a username length 0"
  end

  test "User must have username length less than 16" do
  	user = User.new( username: "abcdefghijklmnopq", password: "testPassword", password_confirmation: "testPassword", first_name: "test", last_name: "student", email: "testStudent@ksu.edu", email_confirmation: "testStudent@ksu.edu")
  	assert_not user.save, "Saved post with a username length > 16"
  end

  test "User must have unique username" do
  	first_user = User.new( username: "testUser", password: "testPassword", password_confirmation: "testPassword", first_name: "test", last_name: "student", email: "testStudent@ksu.edu", email_confirmation: "testStudent@ksu.edu")
  	first_user.save
   	second_user = User.new( username: "testUser", password: "testPassword", password_confirmation: "testPassword", first_name: "test", last_name: "student", email: "differentEmail@ksu.edu", email_confirmation: "differentEmail@ksu.edu")
  	assert_not second_user.save, "Saved user with duplicate username"
  end

  test "User must have alphanumeric username" do
  	user = User.new( username: "abc.", password: "testPassword", password_confirmation: "testPassword", first_name: "test", last_name: "student", email: "testStudent@ksu.edu", email_confirmation: "testStudent@ksu.edu")
  	assert_not user.save, "Saved a user with non-alphanumeric username"
  end

# End Username Assertions

# Begin Password Assertions

  # validates :password, length: { in: 6..20 }, allow_blank: false, on: :create
  # validates :password, format: /\A[0-9A-Za-z_-]+\z/, on: :create
  # validates :password, presence: true, on: :create
  # validates :password, :confirmation => true
  # validates :password_confirmation, :presence => true

  test "User must have a password" do
  	user = User.new( username: "testUser", password_confirmation: "testPassword", first_name: "test", last_name: "student", email: "testStudent@ksu.edu", email_confirmation: "testStudent@ksu.edu")
  	assert_not user.save, "Saved user without a password"
  end  

  test "User must have an alphanumeric password" do
  	user = User.new( username: "testUser", password: "testPassword?", password_confirmation: "testPassword?", first_name: "test", last_name: "student", email: "testStudent@ksu.edu", email_confirmation: "testStudent@ksu.edu")
  	assert_not user.save, "Saved user without an alphanumeric password"
  end  

  test "User must have password of minimum length 6" do
  	user = User.new( username: "testUser", password: "test", password_confirmation: "test", first_name: "test", last_name: "student", email: "testStudent@ksu.edu", email_confirmation: "testStudent@ksu.edu")
  	assert_not user.save, "Saved user with a password less than 6 characters"
  end   

  test "User must have password of maximum length 20" do
  	user = User.new( username: "testUser", password: "thisIsATestPasswordOfMoreThanTwentyCharacters", password_confirmation: "thisIsATestPasswordOfMoreThanTwentyCharacters", first_name: "test", last_name: "student", email: "testStudent@ksu.edu", email_confirmation: "testStudent@ksu.edu")
  	assert_not user.save, "Saved user with a password more than 20 characters"
  end  	

  test "User password and password_confirmation must match" do
    user = User.new( username: "testUser", password: "testPassword", password_confirmation: "notTestPassword", first_name: "test", last_name: "student", email: "testStudent@ksu.edu", email_confirmation: "testStudent@ksu.edu")
    assert_not user.save, "Saved a user whose password and password_confirmation don't match"
  end

  test "User password_confirmation must exist" do
    user = User.new( username: "testUser", password: "testPassword", first_name: "test", last_name: "student", email: "testStudent@ksu.edu", email_confirmation: "testStudent@ksu.edu")
    assert_not user.save, "Saved a user without a password_confirmation"
  end
# End Password Assertions


# Begin First Name Assertions
  # validates :first_name, length: { in: 1..32 }, allow_blank: false, on: :create
  # validates :first_name, presence: true, on: :create
  # validates :first_name, format: /\A[0-9A-Za-z_-]+\z/, on: :create
  test "User first_name length must be greater than 1 characters" do
    user = User.new( username: "testUser", password: "testPassword", password_confirmation: "testPassword", first_name: "", last_name: "student", email: "testStudent@ksu.edu", email_confirmation: "testStudent@ksu.edu")
    assert_not user.save, "Saved a user with a 0 character first name"
  end

  test "User first_name length must be no greater than 32 characters" do
    user = User.new( username: "testUser", password: "testPassword", password_confirmation: "testPassword", first_name: "thisIsAFirstNameOfMoreThanThirtyTwoCharacters", last_name: "student", email: "testStudent@ksu.edu", email_confirmation: "testStudent@ksu.edu")
    assert_not user.save, "Saved a user with a first name greater than 32 characters"
  end

  test "User first_name must exist" do
    user = User.new( username: "testUser", password: "testPassword", password_confirmation: "testPassword", last_name: "student", email: "testStudent@ksu.edu", email_confirmation: "testStudent@ksu.edu")
    assert_not user.save, "Saved a user with a 0 character first name"
  end

  test "User first_name must be alphanumeric" do
    user = User.new( username: "testUser", password: "testPassword", password_confirmation: "testPassword", first_name: "testFirstName?", last_name: "student", email: "testStudent@ksu.edu", email_confirmation: "testStudent@ksu.edu")
    assert_not user.save, "Saved a user with a non-alphanumeric first name"
  end

# End First Name Assertions

# Begin Last Name Assertions
  # validates :last_name, length: { in: 1..32 }, allow_blank: false, on: :create
  # validates :last_name, presence: true, on: :create
  # validates :last_name, format: /\A[0-9A-Za-z_-]+\z/, on: :create
  test "User last_name length must be greater than 1 characters" do
    user = User.new( username: "testUser", password: "testPassword", password_confirmation: "testPassword", first_name: "test", last_name: "", email: "testStudent@ksu.edu", email_confirmation: "testStudent@ksu.edu")
    assert_not user.save, "Saved a user with a 0 character last name"
  end

  test "User last_name length must be no greater than 32 characters" do
    user = User.new( username: "testUser", password: "testPassword", password_confirmation: "testPassword", first_name: "test", last_name: "thisIsALastNameOfMoreThanThirtyTwoCharacters", email: "testStudent@ksu.edu", email_confirmation: "testStudent@ksu.edu")
    assert_not user.save, "Saved a user with a last name greater than 32 characters"
  end

  test "User last_name must exist" do
    user = User.new( username: "testUser", password: "testPassword", password_confirmation: "testPassword", first_name: "test", email: "testStudent@ksu.edu", email_confirmation: "testStudent@ksu.edu")
    assert_not user.save, "Saved a user with a 0 character last name"
  end

  test "User last_name must be alphanumeric" do
    user = User.new( username: "testUser", password: "testPassword", password_confirmation: "testPassword", first_name: "test", last_name: "testLastName?", email: "testStudent@ksu.edu", email_confirmation: "testStudent@ksu.edu")
    assert_not user.save, "Saved a user with a non-alphanumeric last name"
  end

# End Last Name Assertions


# Begin Email Assertions

  # validates :email, uniqueness: true
  # validates :email, presence: true, on: :create
  # validates :email, format: /\A[A-Za-z0-9._-]+@(ksu|k-state)\.[A-Za-z]{2,4}\z/i, on: :create
  # validates :email, :confirmation => true
  # validates :email_confirmation, :presence => true
  test "User email must be unique" do
    first_user = User.new( username: "testUser", password: "testPassword", password_confirmation: "testPassword", first_name: "test", last_name: "student", email: "testStudent@ksu.edu", email_confirmation: "testStudent@ksu.edu")
    first_user.save
    second_user = User.new( username: "testUser2", password: "testPassword", password_confirmation: "testPassword", first_name: "test", last_name: "student", email: "testStudent@ksu.edu", email_confirmation: "testStudent@ksu.edu")
    assert_not second_user.save, "Saved user with duplicate email"
  end

  test "User email must be present" do
    user = User.new( username: "testUser", password: "testPassword", password_confirmation: "testPassword", first_name: "test", last_name: "student", email_confirmation: "testStudent@ksu.edu")
    assert_not user.save, "Saved a user without an email"
  end

  test "User email must be alphanumeric, and have an @ksu/@k-state email." do
    alphaUser = User.new( username: "testUser", password: "testPassword", password_confirmation: "testPassword", first_name: "test", last_name: "student", email: "testStudent?@ksu.edu", email_confirmation: "testStudent?@ksu.edu")
    notksuUser   = User.new( username: "testUser", password: "testPassword", password_confirmation: "testPassword", first_name: "test", last_name: "student", email: "testStudent@notksu.edu", email_confirmation: "testStudent@notksu.edu")
    assert_not alphaUser.save, "Saved a user without a non-alphanumeric email"
    assert_not notksuUser.save, "Saved a user without a ksu/k-state email"
  end

  test "User email must match email_confirmation" do
    user = User.new( username: "testUser", password: "testPassword", password_confirmation: "testPassword", first_name: "test", last_name: "student", email: "testStudent@ksu.edu", email_confirmation: "testStudentFail@ksu.edu")
    assert_not user.save, "Saved a user without matching email and email_confirmation"
  end

  test "User email must have email_confirmation" do
    user = User.new( username: "testUser", password: "testPassword", password_confirmation: "testPassword", first_name: "test", last_name: "student", email: "testStudent@ksu.edu")
    assert_not user.save, "Saved a user without matching email_confirmation"
  end



# End Email Assertions



end

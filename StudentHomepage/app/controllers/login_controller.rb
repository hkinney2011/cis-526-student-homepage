class LoginController < ApplicationController
  def new
    @user = User.new
  end

  def create
    user = User.authenticate(params[:username], params[:password])
    if user
      session[:user_id] = user.id
      redirect_to session_path
    else
      flash.now[:invalid_login] = "Invalid Username or Password!"
      render :new
    end
  end

  def destroy
    session[:user_id] = nil
    flash[:logged_out] = "User Has Been Logged Out"
    redirect_to root_url
  end
end

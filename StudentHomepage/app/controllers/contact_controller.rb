class ContactController < ApplicationController

  def new
    @message = Message.new
  end

  def create
    @message = Message.new(params[:message])
    
    if @message.valid?
      UserMailer.forgot_password(@message).deliver
      redirect_to login_path
    else
      flash.now.alert = "Please fill all fields."
      render :new
    end
  end

end
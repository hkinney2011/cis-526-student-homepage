class UsersController < ApplicationController
  def new
    @user = User.new
  end

  def create
    # This line is for the form_for formbuilder.
    # @user = User.new(params.require(:user).permit(:username, :first_name, :last_name, :password, :password_confirmation, :email, :email_confirmation))

    @user = User.new(params[:user])
    @user.username = params[:user_name]
    @user.first_name = params[:first_name]
    @user.last_name = params[:last_name]
    @user.password = params[:password]
    @user.password_confirmation = params[:password_confirmation]
    @user.email = params[:email]
    @user.email_confirmation = params[:email_confirmation]

    if @user.save
      flash[:registered] = "User Successfully Created"
      redirect_to login_path
    else
      flash[:not_registered] = "ERROR Creating User!"
      redirect_to login_path
    end
  end

end

class HomeController < ApplicationController
  # function to render all users

  def index
    set_cache_buster
    if session[:user_id] == nil
      redirect_to root_path
    else
      @users = User.all
      @clubs = Club.all
      @courses = Course.all
      @myCourses = User.find(1).courses
      @myClubs = User.find(1).clubs
    end
  end
end

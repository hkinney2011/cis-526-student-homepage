class EnrollmentValidator < ActiveModel::Validator
  def validate(record)
 	if record.new_record? && Enrollment.where("user_id = ? AND course_id= ?", record.user_id, record.course_id).exists?
          record.errors.add(:base, 'This enrollment already exists.')
	end
  end
end 

class Enrollment < ActiveRecord::Base
  # attr_accessible :title, :description, :meetingTime, :id
  belongs_to :user
  belongs_to :course
  
  validates_with EnrollmentValidator
end

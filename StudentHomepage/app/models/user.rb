class UserNameValidator < ActiveModel::Validator
  def validate(record)
 	if record.new_record? && User.where(username: record.username).exists?
          record.errors.add(:base, 'This username already exists.')
	end
  end
end 

class User < ActiveRecord::Base
  attr_accessor :password, :password_confirmation, :email_confirmation
  before_save :encrypt_password
  after_save :clear_attributes
  
  has_many :enrollments
  has_many :subscriptions
  has_many :clubs, :through => :subscriptions
  has_many :courses, :through => :enrollments
  
  
  validates :username, uniqueness: true
  validates :username, length: { in: 1..16 }, allow_blank: false, on: :create
  validates :username, presence: true, on: :create
  validates :username, format: /\A[0-9A-Za-z_-]+\z/, on: :create
  validates_with UserNameValidator

  validates :password, length: { in: 6..20 }, allow_blank: false, on: :create
  validates :password, format: /\A[0-9A-Za-z_-]+\z/, on: :create
  validates :password, presence: true, on: :create
  validates :password, :confirmation => true
  validates :password_confirmation, :presence => true

  validates :first_name, length: { in: 1..32 }, allow_blank: false, on: :create
  validates :first_name, presence: true, on: :create
  validates :first_name, format: /\A[0-9A-Za-z_-]+\z/, on: :create

  validates :last_name, length: { in: 1..32 }, allow_blank: false, on: :create
  validates :last_name, presence: true, on: :create
  validates :last_name, format: /\A[0-9A-Za-z_-]+\z/, on: :create

  validates :email, uniqueness: true
  validates :email, presence: true, on: :create
  validates :email, format: /\A[A-Za-z0-9._-]+@(ksu|k-state)\.[A-Za-z]{2,4}\z/i, on: :create
  validates :email, :confirmation => true
  validates :email_confirmation, :presence => true

  def self.authenticate(username, password)
    user = find_by_username(username)
    if user && user.password_hash == BCrypt::Engine.hash_secret(password, user.password_salt)
      user
    else
      nil
    end
  end

  def encrypt_password
    if password.present?
      self.password_salt = BCrypt::Engine.generate_salt
      self.password_hash = BCrypt::Engine.hash_secret(password, password_salt)
    end
  end

  def clear_attributes
    self.password = nil
    self.password_confirmation = nil
    self.email_confirmation = nil
  end
  
  # def user_params
#     # params.require(:person).permit(:name, :age, pets_attributes: [:name, :category])
#     # params.require(:user).permit(:first_name, :last_name, course_attributes: [:title, :description])
#   end
end

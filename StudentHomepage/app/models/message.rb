class Message

  include ActiveModel::Validations
  include ActiveModel::Conversion
  extend ActiveModel::Naming

  attr_accessor :email

  validates :email, presence: true, on: :create
  validates :email, format: /\A[A-Za-z0-9._-]+@(ksu|k-state)\.[A-Za-z]{2,4}\z/i, on: :create


  def initialize(attributes = {})
    attributes.each do |name, value|
      send("#{name}=", value)
    end
  end 

  def persisted?
    false
  end

end
$(document).ready(function() {
    $("#forgot_pass_form").validate({
      rules: {
          "Message[email]": {
            email: true,
            required: true,
            minlength: 1,
            maxlength: 20,
          },
      },
  });
});
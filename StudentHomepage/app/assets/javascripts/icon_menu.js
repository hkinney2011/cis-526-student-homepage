$(document).ready(function() {
  $( '.user_icon' ).unbind('click').click(function() {
    if ($( '#forgot_password_container').is(":visible")) {
      $( '#forgot_password_container' ).slideToggle();
    }
    else if ($( '#register_container' ).is(":visible")) {
      $('#register_container .up_arrow').hide();
      $('#register_container .down_arrow').hide();
      $( '#register_container' ).slideToggle();    
      setTimeout(function(){    $( '#login_container' ).slideToggle( 'slow' );},500 );
    }
    else if ($( '#map_container' ).is(":visible")) {
      $( '#map_container' ).slideToggle();    
      setTimeout(function(){    $( '#login_container' ).slideToggle( 'slow' );},500 );
    }
    else if ($( '#professors_container' ).is(":visible")) {
      $( '#professors_container' ).slideToggle();    
      setTimeout(function(){    $( '#login_container' ).slideToggle( 'slow' );},500 );
    }
    else if ($( '#clubs_container' ).is(":visible")) {
      $( '#clubs_container' ).slideToggle();    
      setTimeout(function(){    $( '#login_container' ).slideToggle( 'slow' );},500 );
    }
    else if ($( '#classes_container' ).is(":visible")) {
      $( '#classes_container' ).slideToggle();    
      setTimeout(function(){    $( '#login_container' ).slideToggle( 'slow' );},500 );
    }
    else if ($( '#events_container' ).is(":visible")) {
      $( '#events_container' ).slideToggle();    
      setTimeout(function(){    $( '#login_container' ).slideToggle( 'slow' );},500 );
    }
    else {
      $( '#login_container' ).slideToggle( 'slow' );
    }
  });
});

$(document).ready(function() {
  $( '#forgot_password_button' ).unbind('click').click(function() {
    $( '#login_container' ).slideToggle( 'slow' );
    $( '#forgot_password_container' ).slideToggle( 'slow' );
  })
})

$(document).ready(function() {
  $( '.add_user_icon' ).unbind('click').click(function() {
    $('#register_container .up_arrow').hide();
    $('#register_container .down_arrow').hide();
    if ($( '#login_container' ).is(":visible")) {
      $( '#login_container' ).slideToggle();    
      setTimeout(function(){    $( '#register_container' ).slideToggle('slow', function(){
        if($(this).scrollTop() == 0){
          $('#register_container .up_arrow').hide();
          $('#register_container .down_arrow').show();
        }
        else if($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight){
          $('#register_container .up_arrow').show();
          $('#register_container .down_arrow').hide();
        }
        else{
          $('#register_container .up_arrow').show();
          $('#register_container .down_arrow').show();
        }
        $('#register_container').scroll(function() {
          if($(this).scrollTop() == 0){
            $('#register_container .up_arrow').hide();
            $('#register_container .down_arrow').show();
          }
          else if($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight){
            $('#register_container .up_arrow').show();
            $('#register_container .down_arrow').hide();
          }
          else{
            $('#register_container .up_arrow').show();
            $('#register_container .down_arrow').show();
          }
        });  
      });},500 );
    }
    else if ($( '#forgot_password_container').is(":visible")) {
      $( '#forgot_password_container' ).slideToggle();
      setTimeout(function(){    $( '#register_container' ).slideToggle('slow', function(){
        if($(this).scrollTop() == 0){
          $('#register_container .up_arrow').hide();
          $('#register_container .down_arrow').show();
        }
        else if($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight){
          $('#register_container .up_arrow').show();
          $('#register_container .down_arrow').hide();
        }
        else{
          $('#register_container .up_arrow').show();
          $('#register_container .down_arrow').show();
        }
        $('#register_container').scroll(function() {
          if($(this).scrollTop() == 0){
            $('#register_container .up_arrow').hide();
            $('#register_container .down_arrow').show();
          }
          else if($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight){
            $('#register_container .up_arrow').show();
            $('#register_container .down_arrow').hide();
          }
          else{
            $('#register_container .up_arrow').show();
            $('#register_container .down_arrow').show();
          }
        });
      });},500);
    }
    else if ($( '#map_container' ).is(":visible")) {
      $( '#map_container' ).slideToggle();
      $('#register_container .up_arrow').hide();
      $('#register_container .down_arrow').hide();    
      setTimeout(function(){    $( '#register_container' ).slideToggle('slow', function(){
        if($(this).scrollTop() == 0){
          $('#register_container .up_arrow').hide();
          $('#register_container .down_arrow').show();
        }
        else if($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight){
          $('#register_container .up_arrow').show();
          $('#register_container .down_arrow').hide();
        }
        else{
          $('#register_container .up_arrow').show();
          $('#register_container .down_arrow').show();
        }
        $('#register_container').scroll(function() {
          if($(this).scrollTop() == 0){
            $('#register_container .up_arrow').hide();
            $('#register_container .down_arrow').show();
          }
          else if($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight){
            $('#register_container .up_arrow').show();
            $('#register_container .down_arrow').hide();
          }
          else{
            $('#register_container .up_arrow').show();
            $('#register_container .down_arrow').show();
          }
        });
      });},500 );
    }
    else {
      $( '#register_container' ).slideToggle('slow', function(){
        if($(this).scrollTop() == 0){
          $('#register_container .up_arrow').hide();
          $('#register_container .down_arrow').show();
        }
        else if($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight){
          $('#register_container .up_arrow').show();
          $('#register_container .down_arrow').hide();
        }
        else{
          $('#register_container .up_arrow').show();
          $('#register_container .down_arrow').show();
        }
        $('#register_container').scroll(function() {
          if($(this).scrollTop() == 0){
            $('#register_container .up_arrow').hide();
            $('#register_container .down_arrow').show();
          }
          else if($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight){
            $('#register_container .up_arrow').show();
            $('#register_container .down_arrow').hide();
          }
          else{
            $('#register_container .up_arrow').show();
            $('#register_container .down_arrow').show();
          }
        });
      });
    }
  });
});

$(document).ready(function() {
  $( '.map_icon' ).unbind('click').click(function() {
    if ($( '#login_container' ).is(":visible")) {
      $( '#login_container' ).slideToggle();    
      setTimeout(function(){    $( '#map_container' ).slideToggle( 'slow' );},500 );
    }
    else if ($( '#forgot_password_container').is(":visible")) {
      $( '#forgot_password_container' ).slideToggle();
      setTimeout(function(){    $( '#map_container' ).slideToggle( 'slow' );},500);
    }
    else if ($( '#register_container' ).is(":visible")) {
      $('#register_container .up_arrow').hide();
      $('#register_container .down_arrow').hide();
      $( '#register_container' ).slideToggle();    
      setTimeout(function(){    $( '#map_container' ).slideToggle( 'slow' );},500 );
    }
    else if ($( '#professors_container' ).is(":visible")) {
      $( '#professors_container' ).slideToggle();    
      setTimeout(function(){    $( '#map_container' ).slideToggle( 'slow' );},500 );
    }
    else if ($( '#clubs_container' ).is(":visible")) {
      $( '#clubs_container' ).slideToggle();    
      setTimeout(function(){    $( '#map_container' ).slideToggle( 'slow' );},500 );
    }
    else if ($( '#classes_container' ).is(":visible")) {
      $( '#classes_container' ).slideToggle();    
      setTimeout(function(){    $( '#map_container' ).slideToggle( 'slow' );},500 );
    }
    else if ($( '#events_container' ).is(":visible")) {
      $( '#events_container' ).slideToggle();    
      setTimeout(function(){    $( '#map_container' ).slideToggle( 'slow' );},500 );
    }
    else {
      $( '#map_container' ).slideToggle( 'slow' );
    }
  });
});

$(document).ready(function() {
  $( '.groups_icon' ).unbind('click').click(function() {
    if ($( '#login_container' ).is(":visible")) {
      $( '#login_container' ).slideToggle();    
      setTimeout(function(){    $( '#clubs_container' ).slideToggle( 'slow' );},500 );
    }
    else if ($( '#forgot_password_container').is(":visible")) {
      $( '#forgot_password_container' ).slideToggle();
      setTimeout(function(){    $( '#clubs_container' ).slideToggle( 'slow' );},500);
    }
    else if ($( '#register_container' ).is(":visible")) {
      $( '#register_container' ).slideToggle();    
      setTimeout(function(){    $( '#clubs_container' ).slideToggle( 'slow' );},500 );
    }
    else if ($( '#professors_container' ).is(":visible")) {
      $( '#professors_container' ).slideToggle();    
      setTimeout(function(){    $( '#clubs_container' ).slideToggle( 'slow' );},500 );
    }
    else if ($( '#map_container' ).is(":visible")) {
      $( '#map_container' ).slideToggle();    
      setTimeout(function(){    $( '#clubs_container' ).slideToggle( 'slow' );},500 );
    }
    else if ($( '#events_container' ).is(":visible")) {
      $( '#events_container' ).slideToggle();    
      setTimeout(function(){    $( '#clubs_container' ).slideToggle( 'slow' );},500 );
    }
    else if ($( '#classes_container' ).is(":visible")) {
      $( '#classes_container' ).slideToggle();    
      setTimeout(function(){    $( '#clubs_container' ).slideToggle( 'slow' );},500 );
    }
    else {
      $( '#clubs_container' ).slideToggle( 'slow' );
    }
  });
});


$(document).ready(function() {
  $( '.classes_icon' ).unbind('click').click(function() {
    if ($( '#login_container' ).is(":visible")) {
      $( '#login_container' ).slideToggle();    
      setTimeout(function(){    $( '#classes_container' ).slideToggle( 'slow' );},500 );
    }
    else if ($( '#forgot_password_container').is(":visible")) {
      $( '#forgot_password_container' ).slideToggle();
      setTimeout(function(){    $( '#classes_container' ).slideToggle( 'slow' );},500);
    }
    else if ($( '#register_container' ).is(":visible")) {
      $( '#register_container' ).slideToggle();    
      setTimeout(function(){    $( '#classes_container' ).slideToggle( 'slow' );},500 );
    }
    else if ($( '#professors_container' ).is(":visible")) {
      $( '#professors_container' ).slideToggle();    
      setTimeout(function(){    $( '#classes_container' ).slideToggle( 'slow' );},500 );
    }
    else if ($( '#map_container' ).is(":visible")) {
      $( '#map_container' ).slideToggle();    
      setTimeout(function(){    $( '#classes_container' ).slideToggle( 'slow' );},500 );
    }
    else if ($( '#clubs_container' ).is(":visible")) {
      $( '#clubs_container' ).slideToggle();    
      setTimeout(function(){    $( '#classes_container' ).slideToggle( 'slow' );},500 );
    }
    else if ($( '#events_container' ).is(":visible")) {
      $( '#events_container' ).slideToggle();    
      setTimeout(function(){    $( '#classes_container' ).slideToggle( 'slow' );},500 );
    }
    else {
      $( '#classes_container' ).slideToggle( 'slow' );
    }
  });
});

$(document).ready(function() {
  $( '.instructors_icon' ).unbind('click').click(function() {

    if ($( '#login_container' ).is(":visible")) {
      $( '#login_container' ).slideToggle();    
      setTimeout(function(){    $( '#professors_container' ).slideToggle( 'slow' );},500 );
    }
    else if ($( '#forgot_password_container').is(":visible")) {
      $( '#forgot_password_container' ).slideToggle();
      setTimeout(function(){    $( '#professors_container' ).slideToggle( 'slow' );},500);
    }
    else if ($( '#register_container' ).is(":visible")) {
      $( '#register_container' ).slideToggle();    
      setTimeout(function(){    $( '#professors_container' ).slideToggle( 'slow' );},500 );
    }
    else if ($( '#clubs_container' ).is(":visible")) {
      $( '#clubs_container' ).slideToggle();    
      setTimeout(function(){    $( '#professors_container' ).slideToggle( 'slow' );},500 );
    }
    else if ($( '#map_container' ).is(":visible")) {
      $( '#map_container' ).slideToggle();    
      setTimeout(function(){    $( '#professors_container' ).slideToggle( 'slow' );},500 );
    }
    else if ($( '#classes_container' ).is(":visible")) {
      $( '#classes_container' ).slideToggle();    
      setTimeout(function(){    $( '#professors_container' ).slideToggle( 'slow' );},500 );
    }
    else if ($( '#events_container' ).is(":visible")) {
      $( '#events_container' ).slideToggle();    
      setTimeout(function(){    $( '#professors_container' ).slideToggle( 'slow' );},500 );
    }
    else {
      $( '#professors_container' ).slideToggle( 'slow' );
    }
  });
});

$(document).ready(function() {
  $( '.events_icon' ).unbind('click').click(function() {

    if ($( '#login_container' ).is(":visible")) {
      $( '#login_container' ).slideToggle();    
      setTimeout(function(){    $( '#events_container' ).slideToggle( 'slow' );},500 );
    }
    else if ($( '#forgot_password_container').is(":visible")) {
      $( '#forgot_password_container' ).slideToggle();
      setTimeout(function(){    $( '#events_container' ).slideToggle( 'slow' );},500);
    }
    else if ($( '#register_container' ).is(":visible")) {
      $( '#register_container' ).slideToggle();    
      setTimeout(function(){    $( '#events_container' ).slideToggle( 'slow' );},500 );
    }
    else if ($( '#clubs_container' ).is(":visible")) {
      $( '#clubs_container' ).slideToggle();    
      setTimeout(function(){    $( '#events_container' ).slideToggle( 'slow' );},500 );
    }
    else if ($( '#map_container' ).is(":visible")) {
      $( '#map_container' ).slideToggle();    
      setTimeout(function(){    $( '#events_container' ).slideToggle( 'slow' );},500 );
    }
    else if ($( '#classes_container' ).is(":visible")) {
      $( '#classes_container' ).slideToggle();    
      setTimeout(function(){    $( '#events_container' ).slideToggle( 'slow' );},500 );
    }
    else if ($( '#professors_container' ).is(":visible")) {
      $( '#professors_container' ).slideToggle();    
      setTimeout(function(){    $( '#events_container' ).slideToggle( 'slow' );},500 );
    }
    else {
      $( '#events_container' ).slideToggle( 'slow' );
    }
  });
});

$(document).ready(function() {
  $(window).on('orientationchange', function(){
    if($('#sticky_footer_home')[0].scrollWidth > $(window).width()){
      if($('#sticky_footer_home').scrollLeft() == 0){
        $('.left_arrow_icon').hide();
        $('.right_arrow_icon').show();
      }
      else if($('#sticky_footer_home').scrollLeft() + $('#sticky_footer_home').innerWidth() >= $('#sticky_footer_home')[0].scrollWidth){
        $('.left_arrow_icon').show();
        $('.right_arrow_icon').hide();
      }
      else{
        $('.left_arrow_icon').show();
        $('.right_arrow_icon').show();
      }
    }
    else{
      $('.right_arrow_icon').hide();
      $('.left_arrow_icon').hide();
    }
    $('body').css({
      "background":"url('../images/StudentPortalBackground.jpg') no-repeat",
      "-webkit-background-size":"cover",
      "-moz-background-size":"cover",
      "-o-background-size":"cover",
      "background-size":"cover",
      "width":"100%",
      "height":"100%",
      "background-color":"#50425E",
      "overflow-y":"hidden"
    });
    $('#header').css({
      "padding":"0",
      "margin":"0",
      "width":"97%",
      "height":"135px",
      "background-image":"url('../images/StudentPortalHeader.png')",
      "background-size":"contain",
      "background-repeat":"no-repeat"
    });
    $('#footer').css({
      "background":"#B9B9B9",
      "bottom":"0",
      "height":"70px",
      "left":"0",
      "position":"fixed",
      "width":"100%"
    });
    $('#sticky_footer').css({
      "line-height":"70px",
      "margin":"0 auto",
      "width":"100%",
      "text-align":"center",
      "overflow-x":"scroll"
    });
    $('#sticky_footer_home').css({
      "line-height":"70px",
      "margin":"0 auto",
      "width":"100%",
      "text-align":"left",
      "overflow-x":"scroll"
    });
    $('#footer img').css({
      "vertical-align":"middle"
    });
    $('#icons').css({
      "width":"200%"
    });
  });
});

$(document).ready(function() {
  if ($( '#sticky_footer_home').is(":visible")) {
    if($('#sticky_footer_home')[0].scrollWidth > $(window).width()){
      if($('#sticky_footer_home').scrollLeft() == 0){
        $('.left_arrow_icon').hide();
        $('.right_arrow_icon').show();
      }
      else if($('#sticky_footer_home').scrollLeft() + $('#sticky_footer_home').innerWidth() >= $('#sticky_footer_home')[0].scrollWidth){
        $('.left_arrow_icon').show();
        $('.right_arrow_icon').hide();
      }
      else{
        $('.left_arrow_icon').show();
        $('.right_arrow_icon').show();
      }
    }
    else{
      $('.right_arrow_icon').hide();
      $('.left_arrow_icon').hide();
    }
    $('#sticky_footer_home').scroll(function() {
      if($(this).scrollLeft() == 0){
        $('.left_arrow_icon').hide();
        $('.right_arrow_icon').show();
      }
      else if($(this).scrollLeft() + $(this).innerWidth() >= $(this)[0].scrollWidth){
        $('.left_arrow_icon').show();
        $('.right_arrow_icon').hide();
	  }
      else{
        $('.left_arrow_icon').show();
        $('.right_arrow_icon').show();
      }
    });
  }
});

$(document).ready(function() {
  $('#login_container .cancel_button').unbind('click').click(function() {
    $('#login_container .form').val('');
    $('#login_container').slideToggle('slow');
  });
});

$(document).ready(function() {
  $('#register_container .cancel_button').unbind('click').click(function() {
    $('#register_container .form').val('');
    $('#register_container .up_arrow').hide();
    $('#register_container .down_arrow').hide();
    $('#register_container').slideToggle('slow');
  });
});

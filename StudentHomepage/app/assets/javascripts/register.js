$(document).ready(function() {
    $("#new_user").validate({
        rules: {
          user_name: {
            required: true,
            minlength: 1,
            maxlength: 16,
          },

          password: {
            required: true,
            minlength: 6,
            maxlength: 20,
            equalTo: password_confirmation,
          },

          first_name: {
            required: true,
            minlength: 1,
            maxlength: 32,
          },

          last_name: {
            required: true,
            minlength: 1,
            maxlength: 32,
          },

          email: {
            email: true,
            required: true,
            minlength: 1,
            maxlength: 20,
          },

          email_confirmation: {
            equalTo: email,
          },
        },

        errorPlacement: function(label, element) {
            label.prepend('<br>');
            label.insertAfter(element);
        },
        wrapper: 'span'
    });
});

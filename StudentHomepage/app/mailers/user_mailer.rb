class UserMailer < ActionMailer::Base
  	default from: "cis526forgotpass@gmail.com"


	def forgot_password( message )
		@message = message
		mail( :to => message.email,  :subject => "[CIS Student Portal] Forgot Password" ).deliver
	end

end
require 'tlsmail'
NET::SMPT.enable_tls(OpenSSL::SSL::VERIFY_NONE)

ActionMailer::Base.perform_deliveries = true
ActionMailer::Base.delivery_method = :smtp
ActionMailer::Base.smtp_settings = {
  :enable_starttls_auto => true
  :address          => "smtp.gmail.com",
  :port             => 587,
  :user_name        => "cis526forgotpass@gmail.com",
  :password         => "ksupassword",
  :authentication   => :plain,
}


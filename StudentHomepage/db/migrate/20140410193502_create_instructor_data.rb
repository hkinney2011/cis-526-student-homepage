class CreateInstructorData < ActiveRecord::Migration
  def change
    create_table :instructor_data do |t|
      t.string :name
      t.string :phone_number
      t.string :hours
      t.string :email
      t.string :website

      t.timestamps
    end
  end
end

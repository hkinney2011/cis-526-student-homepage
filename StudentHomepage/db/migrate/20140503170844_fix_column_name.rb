class FixColumnName < ActiveRecord::Migration
  def change
    # rename_column :users, :id, :user_id
    # rename_column :courses, :id, :course_id
    rename_column :enrollments, :course_number, :course_id
    add_column :users, :user_id, :integer
    add_column :courses, :course_id, :integer
    add_column :clubs, :president, :string
    add_column :clubs, :meetingTime, :string
    add_column :clubs, :club_id, :integer
    add_column :clubs, :website, :string
  end
end

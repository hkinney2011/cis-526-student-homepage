class CreateInstructors < ActiveRecord::Migration
  def change
    create_table :instructors do |t|
      t.string :firstName
      t.string :lastName
      t.string :email
      t.string :department

      t.timestamps
    end
  end
end

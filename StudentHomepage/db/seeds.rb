# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

require_relative 'seeds/user_seeds'
require_relative 'seeds/course_seeds'
require_relative 'seeds/enrollment_seeds'
require_relative 'seeds/subscription_seeds'
require_relative 'seeds/club_seeds'

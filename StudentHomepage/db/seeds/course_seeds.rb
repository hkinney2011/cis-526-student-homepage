if Course.where(title: 'CIS 526').blank?
  @c1 = Course.new(
  course_id: '1',
  title: 'CIS 526',
  description: 'Web Interface and Design',
  meetingTime: 'MWF 2:30-3:45'
  ).save(validate: false)
end
  
if Course.where(title: 'CIS 501').blank?
  @c1 = Course.new(
  course_id: '2',
  title:'CIS 501',
  description:'Operating Systems',
  meetingTime:'MWF 1:30-2:20'
  ).save(validate: false)
end

if Course.where(title: 'CIS 200').blank?
  @c1 = Course.new(
  course_id: '3',
  title:'CIS 200',
  description:'Intro to Computer Science',
  meetingTime:'MWF 9:30'
  ).save(validate: false)
end

if Course.where(title: 'CIS 525').blank?
  @c1 = Course.new(
  course_id: '4',
  title:'CIS 525',
  description:'Telecommunications and Networking',
  meetingTime:'MWF 1:30-2:20'
  ).save(validate: false)
end

if Course.where(title: 'CIS 597').blank?
  @c1 = Course.new(
  course_id: '5',
  title:'CIS 597',
  description:'Senior Independent Project',
  meetingTime:'U 1:30'
  ).save(validate: false)
end

if Course.where(title: 'CIS 562').blank?
  @c1 = Course.new(
  course_id: '6',
  title:'CIS 562',
  description:'Enterprise Web Applications',
  meetingTime:'TU 2:30-3:45'
  ).save(validate: false)
end
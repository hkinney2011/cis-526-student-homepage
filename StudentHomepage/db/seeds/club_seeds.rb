# if Course.where(title: 'CIS 526').blank?
#   @c1 = Course.new(
#   course_id: '1',
#   title: 'CIS 526',
#   description: 'Web Interface and Design',
#   meetingTime: 'MWF 2:30-3:45'
#   ).save(validate: false)
# end
if Club.where(name: 'ACM').blank?
  @c1 = Club.new(
  club_id: '1',
  name: 'ACM',
  president: 'Ethan Haley',
  meetingTime: 'Th 6:45-8:00',
  website: 'http://www.acm.cis.ksu.edu/'
  ).save(validate: false)
end

if Club.where(name: 'Video Game Designers').blank?
  @c1 = Club.new(
  club_id: '2',
  name: 'Video Game Designers',
  president: 'Nathan Bean',
  meetingTime: 'W 8:00-10:00',
  website: 'http://www.cis.ksu.edu/'
  ).save(validate: false)
end

if Club.where(name: 'Wildcraft').blank?
  @c1 = Club.new(
  club_id: '3',
  name: 'Wildcraft',
  president: 'Ethan Haley',
  meetingTime: 'MWF 7:45-10:00',
  website: 'http://www.ksuwildcraft.com'
  ).save(validate: false)
end

if Club.where(name: 'IEEE').blank?
  @c1 = Club.new(
  club_id: '4',
  name: 'IEEE',
  president: 'Andy Davic',
  meetingTime: 'M 6:00-8:00',
  website: 'http://www.ieee.org'
  ).save(validate: false)
end
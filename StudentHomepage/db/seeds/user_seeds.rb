if User.where(email: 'abobby@ksu.edu').blank?
 @u1 = User.new(username: 'AliceBobby',
    user_id: '1',
    first_name: 'Alice',
    last_name: 'Bobby',
    password: 'password',
    password_confirmation: 'password',
    email: 'abobby@ksu.edu',
    email_confirmation: 'abobby@ksu.edu').save()
end

if User.where(email: 'cdennis@ksu.edu').blank?
 @u2 = User.new(username: 'CarolDennis',
    user_id: '2',
    first_name: 'Carol',
    last_name: 'Dennis',
    password: 'password',
    password_confirmation: 'password',
    email: 'cdennis@ksu.edu',
    email_confirmation: 'cdennis@ksu.edu'
    ).save()
end

if User.where(email: 'efrank@ksu.edu').blank?
 @u3 = User.new(username: 'EdwardFrank',
    user_id: '3',
    first_name: 'Edward',
    last_name: 'Frank',
    password: 'password',
    password_confirmation: 'password',
    email: 'efrank@ksu.edu',
    email_confirmation: 'efrank@ksu.edu'
    ).save(validate: false)
end

if User.where(email: 'ghughes@ksu.edu').blank?
 @u4 = User.new(username: 'GaryHughes',
    user_id: '4',
    first_name: 'Gary',
    last_name: 'Hughes',
    password: 'password',
    password_confirmation: 'password',
    email: 'ghughes@ksu.edu',
    email_confirmation: 'ghughes@ksu.edu'
    ).save(validate: false)
end

if User.where(email: 'ijenkins@ksu.edu').blank?
 @u5 = User.new(username: 'IrisJenkins',
    user_id: '5',
    first_name: 'Iris',
    last_name: 'Jenkins',
    password: 'password',
    password_confirmation: 'password',
    email: 'ijenkins@ksu.edu',
    email_confirmation: 'ijenkins@ksu.edu'
    ).save(validate: false)
end

if User.where(email: 'teama@ksu.edu').blank?
 @u6 = User.new(username: 'TeamA',
    user_id: '6',
    first_name: 'Test',
    last_name: 'User',
    password: 'password',
    password_confirmation: 'password',
    email: 'teama@ksu.edu',
    email_confirmation: 'teama@ksu.edu'
    ).save(validate: false)
end

if User.where(email: 'teamb@ksu.edu').blank?
 @u15 = User.new(username: 'TeamB',
    user_id: '7',
    first_name: 'Test',
    last_name: 'User',
    password: 'password',
    password_confirmation: 'password',
    email: 'teamb@ksu.edu',
    email_confirmation: 'teamb@ksu.edu'
    ).save(validate: false)
end

if User.where(email: 'teamc@ksu.edu').blank?
 @u7 = User.new(username: 'TeamC',
    user_id: '8',
    first_name: 'Test',
    last_name: 'User',
    password: 'password',
    password_confirmation: 'password',
    email: 'teamc@ksu.edu',
    email_confirmation: 'teamc@ksu.edu'
    ).save(validate: false)
end

if User.where(email: 'teamd@ksu.edu').blank?
 @u8 = User.new(username: 'TeamD',
    user_id: '9',
    first_name: 'Test',
    last_name: 'User',
    password: 'password',
    password_confirmation: 'password',
    email: 'teamd@ksu.edu',
    email_confirmation: 'teamd@ksu.edu'
    ).save(validate: false)
end

if User.where(email: 'teame@ksu.edu').blank?
 @u9 = User.new(username: 'TeamE',
    user_id: '10',
    first_name: 'Test',
    last_name: 'User',
    password: 'password',
    password_confirmation: 'password',
    email: 'teame@ksu.edu',
    email_confirmation: 'teame@ksu.edu'
    ).save(validate: false)
end

if User.where(email: 'teamf@ksu.edu').blank?
 @u10 = User.new(username: 'TeamF',
    user_id: '11',
    first_name: 'Test',
    last_name: 'User',
    password: 'password',
    password_confirmation: 'password',
    email: 'teamf@ksu.edu',
    email_confirmation: 'teamf@ksu.edu'
    ).save(validate: false)
end

if User.where(email: 'teamG@ksu.edu').blank?
 @u11 = User.new(username: 'TeamG',
    user_id: '12',
    first_name: 'Test',
    last_name: 'User',
    password: 'password',
    password_confirmation: 'password',
    email: 'teamg@ksu.edu',
    email_confirmation: 'teamg@ksu.edu'
    ).save(validate: false)
end

if User.where(email: 'teamH@ksu.edu').blank?
 @u12 = User.new(username: 'TeamH',
    user_id: '13',
    first_name: 'Test',
    last_name: 'User',
    password: 'password',
    password_confirmation: 'password',
    email: 'teamh@ksu.edu',
    email_confirmation: 'teamh@ksu.edu'
    ).save(validate: false)
end

if User.where(email: 'teami@ksu.edu').blank?
 @u14 = User.new(username: 'TeamI',
    user_id: '14',
    first_name: 'Test',
    last_name: 'User',
    password: 'password',
    password_confirmation: 'password',
    email: 'teami@ksu.edu',
    email_confirmation: 'teami@ksu.edu'
    ).save(validate: false)
end

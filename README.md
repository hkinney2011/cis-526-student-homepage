# README #

This project should be easily accessed and setup. From inside the directory, assuming rails is installed, run "rails -s" to run on localhost:3000. Seeds for login are available in the db->seeds folder, or you can register on the website. 

If you have problems, running the commands "rake db:drop", "rake db:migrate", "rake db:seed" in the terminal should fix most issues. 

This project was done for Kansas State's CIS 526 course under Nathan Bean and Daniel Andresen, completed by Hayden Kinney, William Hommertzheim, and Adam Steen. 

There are no known issues with this build.